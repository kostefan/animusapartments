<?php

use Illuminate\Database\Seeder;
use App\ApartmentType;

class ApartmentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ApartmentType::create(['type' => 'Single Bedroom']);
        ApartmentType::create(['type' => 'Double Bedroom']);
        ApartmentType::create(['type' => 'Student Single']);
        ApartmentType::create(['type' => 'Student Shared']);
    }
}
