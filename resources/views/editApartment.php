	<!DOCTYPE html>
	<html lang="en-US" ng-app="apartmentRecords">
	    <head>
	        <title>Animus Apartments: Edit Page</title>

	        <!-- Load Bootstrap CSS -->
	        <link href="<?= asset('css/bootstrap.min.css') ?>" rel="stylesheet">
	    </head>
	    <body>

	    	<div  ng-controller="editController">
            	 <form name="frmApartments" class="form-horizontal" novalidate="">

                    <div class="form-group error">
                        <label for="inputEmail3" class="col-sm-3 control-label">Location</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control has-error" id="location" name="location" placeholder="Location Address" value="{{location}}" 
                            ng-model="editApartment.location" ng-required="true">
                            <span class="help-inline" 
                            ng-show="frmApartments.location.$invalid && frmApartments.location.$touched">Location field is required</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Apartment Type</label>
                        <div class="col-sm-9">
                          <select class="form-control" ng-options="item as item.type for item in editTypes track by item.id" ng-model="editApartment.type" ng-required="true"></select>
                          <span class="help-inline"
                            ng-show="frmApartments.type.$invalid && frmApartments.type.$touched">Please select apartment type</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Length</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="length" name="length" placeholder="Length" value="{{length}}" 
                            ng-model="editApartment.length" ng-required="true">
                            <span class="help-inline" 
                            ng-show="frmApartments.length.$invalid && frmApartments.length.$touched">Valid Length field is required</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Breadth</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="breadth" name="breadth" placeholder="Breadth" value="{{breadth}}" 
                            ng-model="editApartment.breadth" ng-required="true">
                            <span class="help-inline" 
                            ng-show="frmApartments.breadth.$invalid && frmApartments.breadth.$touched">Valid Length field is required</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Rent</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="rent" name="rent" placeholder="rent" value="{{rent}}" 
                            ng-model="editApartment.rent" ng-required="true">
                            <span class="help-inline" 
                            ng-show="frmApartments.rent.$invalid && frmApartments.rent.$touched">Rent field is required</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-9">
                            <!-- <input type="text" class="form-control" id="position" name="position" placeholder="Position" value="{{position}}" 
                            ng-model="employee.position" ng-required="true"> -->
                            <textarea class="form-control" id="details" name="details" placeholder="Details" value="{{details}}" 
                            ng-model="editApartment.details" ng-required="true"></textarea>
                        <span class="help-inline" 
                            ng-show="frmApartments.details.$invalid && frmApartments.details.$touched">Details field is required</span>
                        </div>
                    </div>
                    <div class="row">
					    <div class="col-md-2 col-md-offset-5">
							<button type="button" class="btn btn-primary" id="btn-save" ng-click="update(editApartment.id)" ng-disabled="frmApartments.$invalid">Save changes</button>    	
					    </div>
					</div>
                    
                </form>


            </div>
	            
	                        

	            
	        <!-- Load Javascript Libraries (AngularJS, JQuery, Bootstrap) -->
	        <script src="<?= asset('app/lib/angular/angular.min.js') ?>"></script>
	        <script src="<?= asset('app/lib/angular/angular-route.min.js') ?>"></script>
	        <script src="<?= asset('js/jquery-3.1.1.slim.min.js') ?>"></script>
	        <script src="<?= asset('js/bootstrap.min.js') ?>"></script>
	        
	        <!-- AngularJS Application Scripts -->
	        <script src="<?= asset('app/app.js') ?>"></script>
	        <script src="<?= asset('app/controllers/editor.js') ?>"></script>
	    </body>
	</html>