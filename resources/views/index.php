<!DOCTYPE html>
<html lang="en-US" ng-app="apartmentRecords">
    <head>
        <title>Animus : Apartments</title>

        <!-- Load Bootstrap CSS -->
        <link href="<?= asset('css/bootstrap.min.css') ?>" rel="stylesheet">
    </head>
    <body>
        <h2>Apartments List</h2>
        <div  ng-controller="apartmentsController">

            <!-- Table-to-load-the-data Part -->
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Location</th>
                        <th>Dimension</th>
                        <th>Rent</th>
                        <!-- <th>Position</th> -->
                        <th><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Apartment</button></th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="apartment in apartments">
                        <td>{{ apartment["id"] }}</td>
                        <td>{{ apartment["location"] }}</td>
                        <td>{{ apartment["length"] }} X {{ apartment["breadth"] }} </td>
                        <td>{{ apartment["rent"] }}</td>
                        <td>
                            <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('view', apartment['id'])">View</button>
                            <button class="btn btn-danger btn-xs btn-delete" ng-click="confirmDelete(apartment['id'])">Delete</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- End of Table-to-load-the-data Part -->
            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                        </div>
                        <div class="modal-body">
                            <form name="frmApartments" class="form-horizontal" novalidate="">

                                <div class="form-group error">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Location</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="location" name="location" placeholder="Location Address" value="{{location}}" 
                                        ng-model="apartment.location" ng-required="true">
                                        <span class="help-inline" 
                                        ng-show="frmApartments.location.$invalid && frmApartments.location.$touched">Location field is required</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Apartment Type</label>
                                    <div class="col-sm-9">
                                        <!-- <input type="text" class="form-control" id="length" name="length" placeholder="Length" value="{{length}}" 
                                        ng-model="apartment.length" ng-required="true"> -->
                                        <select class="form-control" id="type" name="type_id" ng-model="apartment.type"  ng-required="true">
                                            <option ng-repeat="type in types" value="{{ type.id }}">{{ type.type }}</option>
                                        </select>
                                        <span class="help-inline" 
                                        ng-show="frmApartments.type.$invalid && frmApartments.type.$touched">Please select apartment type</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Length</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="length" name="length" placeholder="Length" value="{{length}}" 
                                        ng-model="apartment.length" ng-required="true">
                                        <span class="help-inline" 
                                        ng-show="frmApartments.length.$invalid && frmApartments.length.$touched">Valid Length field is required</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Breadth</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="breadth" name="breadth" placeholder="Breadth" value="{{breadth}}" 
                                        ng-model="apartment.breadth" ng-required="true">
                                        <span class="help-inline" 
                                        ng-show="frmApartments.breadth.$invalid && frmApartments.breadth.$touched">Valid Length field is required</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Rent</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="rent" name="rent" placeholder="rent" value="{{rent}}" 
                                        ng-model="apartment.rent" ng-required="true">
                                        <span class="help-inline" 
                                        ng-show="frmApartments.rent.$invalid && frmApartments.rent.$touched">Rent field is required</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                                    <div class="col-sm-9">
                                        <!-- <input type="text" class="form-control" id="position" name="position" placeholder="Position" value="{{position}}" 
                                        ng-model="employee.position" ng-required="true"> -->
                                        <textarea class="form-control" id="details" name="details" placeholder="Details" value="{{details}}" 
                                        ng-model="apartment.details" ng-required="true"></textarea>
                                    <span class="help-inline" 
                                        ng-show="frmApartments.details.$invalid && frmApartments.details.$touched">Details field is required</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Email of creator</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{email}}" 
                                        ng-model="apartment.email" ng-required="true">
                                        <!-- <textarea class="form-control" id="email" name="email" placeholder="email" value="{{email}}" 
                                        ng-model="apartment.email" ng-required="true"></textarea> -->
                                    <span class="help-inline" ng-show="frmApartments.email.$invalid && frmApartments.email.$touched">Email field is required</span>
                                    </div>
                                </div>
                                <!-- <input type="hidden" id="token" value="{{ csrf_token() }}"> -->
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(modalstate, id)" ng-disabled="frmApartments.$invalid">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="viewApartmentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Apartment ID</td>
                                        <td>{{ viewApartment.id }}</td>
                                    </tr>
                                    <tr>
                                        <td>Location</td>
                                        <td>{{ viewApartment.location }}</td>
                                    </tr>
                                    <tr>
                                        <td>Apartment Type</td>
                                        <td>{{ viewApartment.type }}</td>
                                    </tr>
                                    <tr>
                                        <td>Length</td>
                                        <td>{{ viewApartment.length }}</td>
                                    </tr>
                                    <tr>
                                        <td>Breadth</td>
                                        <td>{{ viewApartment.breadth }}</td>
                                    </tr>
                                    <tr>
                                        <td>Rent</td>
                                        <td>{{ viewApartment.rent }}</td>
                                    </tr>
                                    <tr>
                                        <td>Description</td>
                                        <td>{{ viewApartment.details }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-cancel" ng-click="cancelView($event)">Cancel</button>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>

        
        </div>

        <!-- Load Javascript Libraries (AngularJS, JQuery, Bootstrap) -->
        <script src="<?= asset('app/lib/angular/angular.min.js') ?>"></script>
        <script src="<?= asset('js/jquery-3.1.1.slim.min.js') ?>"></script>
        <script src="<?= asset('js/bootstrap.min.js') ?>"></script>
        
        <!-- AngularJS Application Scripts -->
        <script src="<?= asset('app/app.js') ?>"></script>
        <script src="<?= asset('app/controllers/apartments.js') ?>"></script>
    </body>
</html>