<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    //
    protected $fillable = array('id', 'location', 'length','breadth','type_id','rent','details');
}
