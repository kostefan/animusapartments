<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ApartmentType;

class ApartmentTypes extends Controller
{
    //
    public function index($id = null) {
        if ($id == null) {
            return ApartmentType::orderBy('id', 'asc')->get();
        } else {
            return $this->show($id);
        }
    }
}
