<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ApartmentCreator;

class ApartmentCreators extends Controller
{
    //
    public function index($id = null) {
        if ($id == null) {
            return ApartmentCreator::orderBy('id', 'asc')->get();
        } else {
            return $this->show($id);
        }
    }
}
