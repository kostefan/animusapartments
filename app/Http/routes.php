<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    /*return view('welcome');*/
    return view('index');
});

Route::get('/test', function () {
    /*return view('welcome');*/
    return view('test');
});

Route::get('/apartment/edit/{id?}', 'EditController@index');


/*
	Routes for Apartments
*/
Route::get('/api/v1/apartments/{id?}', 'Apartments@index');
Route::post('/api/v1/apartments', 'Apartments@store');
Route::post('/api/v1/apartments/{id?}', 'Apartments@update');
Route::delete('/api/v1/apartments/{id?}', 'Apartments@destroy');


/*
	Routes for ApartmentTypes

*/

Route::get('/api/v1/types/{id?}', 'ApartmentTypes@index');
/*Route::post('/api/v1/apartments', 'Apartments@store');
Route::post('/api/v1/apartments/{id}', 'Apartments@update');
Route::delete('/api/v1/apartments/{id}', 'Apartments@destroy');*/


/*
	Routes for AparmentCreators
*/

Route::get('/api/v1/creators/{id?}', 'ApartmentCreators@index');
