var app = angular.module('apartmentRecords', [],function($locationProvider){
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: false
	});
})
.constant('API_URL', 'http://localhost:8000/api/v1/')
.constant("CSRF_TOKEN", '{{ csrf_token() }}');