app.controller('editController', ['$scope', '$http','$location','API_URL', 'CSRF_TOKEN', function($scope, $http, $location, API_URL, CSRF_TOKEN) {

	var apartmentId =  $location.path().split("/")[3]||"Unknown";
	
	$http.get(API_URL + 'apartments/' + apartmentId)
	.then(function onSuccess(response) {
		$scope.editApartment = response["data"][0];
    $scope.editApartment.type = {
      type: response["data"][0].type,
      id: response["data"][0].type_id
    };
		console.log(response["data"][0]);
	});

	$http.get(API_URL + "types")
	.then(function onSuccess(response) {
		$scope.editTypes = response["data"];

	});

	$scope.update = function(id) {
        var url = API_URL + "apartments/" + id;
        
        $http({
            method: 'POST',
            url: url,
            data: {
                    "location"  : $scope.editApartment.location, 
                    "length"    : $scope.editApartment.length, 
                    "breadth"   : $scope.editApartment.breadth,
                    "type_id"   : $scope.editApartment.type.id,
                    "rent"      : $scope.editApartment.rent,
                    "details"   : $scope.editApartment.details
            }
            /*headers: {'Content-Type': 'application/x-www-form-urlencoded'}*/
        }).then(function onSuccess(response) {
            alert('Apartment edited Successfully!!');
            location.reload();
        },
        function onError(response) {
            console.log(response['data']);
            alert('Sorry there has been an error! Please refer to console for error log');
        });

    }

}]);