app.controller('apartmentsController', ['$scope', '$http','API_URL', 'CSRF_TOKEN', function($scope, $http, API_URL, CSRF_TOKEN) {
    //retrieve apartment types listing from API
    $http.get(API_URL + "types")
            .then(function onSuccess(response) {
                    $scope.types = response["data"];
                    
                    /*console.log(JSON.stringify(response));*/
                    console.log(response["data"][0]["id"]);  

                });
    //retrieve apartments list from API
    $http.get(API_URL + "apartments")
            .then(function onSuccess(response) {
                    $scope.apartments = response["data"];
                    /*console.log(response);*/  

                });


    //show modal form
    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Apartment";
                $('#myModal').modal('show');
                break;
            case 'view':
                $scope.form_title = "Apartment "+id+" Detail";
                $scope.id = id;
                $http.get(API_URL + 'apartments/' + id)
                        .then(function onSuccess(response) {
                            console.log(response['data'][0]);
                            $scope.viewApartment = response['data'][0];
                        });
                $('#viewApartmentModal').modal('show');                
                break;
            default:
                break;
        }
        /*console.log(id);*/
        
    }

    //save new record
    $scope.save = function(modalstate, id) {
        var url = API_URL + "apartments";
        
        $http({
            method: 'POST',
            url: url,
            data: {
                    "location"  : $scope.apartment.location, 
                    "length"    : $scope.apartment.length, 
                    "breadth"   : $scope.apartment.breadth,
                    "type_id"   : $scope.apartment.type,
                    "rent"      : $scope.apartment.rent,
                    "details"   : $scope.apartment.details,  
                    "email"     : $scope.apartment.email
            }
            /*headers: {'Content-Type': 'application/x-www-form-urlencoded'}*/
        }).then(function onSuccess(response) {
            alert('Apartment Created Successfully!! Please check your email for the edit link.');
            location.reload();
        },
        function onError(response) {
            console.log(response);
            alert('Sorry there has been an error! Please refer to console for error log');
        });

    }

    //delete record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want delete this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'apartments/' + id
            }).then(function onSuccess(response) {
                console.log(response['data']);
                location.reload();
            },
            function onError(response) {
                console.log(response['data']);
                /*location.reload();*/
                alert('Sorry there has been an error! Please refer to console for error log');
        });
        } else {
            return false;
        }
    }

}]);